import logo from './logo.svg';
import './App.css';
import React,{useState,useEffect} from 'react';


function App() {
  const url="https://jsonplaceholder.typicode.com/users";
  const[data ,setData]=useState([]);

  const fetchInfo=()=>{
    return fetch(url)
    .then((res)=>res.json())
    .then((d)=>setData(d))
  };

  useEffect(()=>{
    fetchInfo();
  })

  return (
    <div className="App">
      <h1 style={{color:'green'}}>using JAVASCRIPT inbuild fetch api</h1>
      <center>
        {data.map((dataObj,index)=>{
          return(
            <div
            style={{ 
              width:'15em',
              background:'#35D841',
              padding:2,
              borderRadius:10,
              marginBlock:10,
             }}
             key={index}
            >
              <p style={{ fontSize:20, color:'#fff' }}>{dataObj.id},{dataObj.username},{dataObj.email}</p>
              <p style={{ fontSize:20,color:'#fff' }}>{dataObj.name}</p>
            </div>
          )
        })}
      </center>
    </div>
  );
}

export default App;
